library(RScaling)
sourcefolder<-getwd()
ptm<-proc.time()
# This file computes the Hs for different parameters
params<-expand.grid(round(seq(-0.9,0.9,0.1),2),round(c(-1,0.,0.5,1.0,1.5),2),2^c(5,6,7,8))
params<-params[-which(params[,2]!=0 & params[,3]!=2^7),]
#params<-expand.grid(round(seq(-0.9,0.9,0.1),2),round(c(-1,0.,0.5),2),2^c(5))
n.params<-length(params[,1])
print(n.params)
n.reps<-3
seedz<-runif(n.reps,min = 1,max = 1111111)
unsinc<-FALSE
LwPss<-TRUE
filename<-"Hresults3FilteredBlocks3TSLengths"
sim.method<-"fn"
defaulttry<-function(expr,default){
  res<-try(expr = expr,silent = TRUE)
  # If the generalized linear model does not converge, or produces an outlier, due to numerical instabilities, we perform a linear model instead
  if(is.numeric(res) & abs(res)<4){return(res)}else{return(default)}
}
# Initializes the matrix for the results
Hresults.sts<-matrix(0,n.params,2*4*n.reps+3)
Hresults.lts<-matrix(0,n.params,2*4*n.reps+3)
Hresults.mts<-matrix(0,n.params,2*4*n.reps+3)
Hresults.ats<-matrix(0,n.params,2*4*n.reps+3)
Hresults.bts<-matrix(0,n.params,2*4*n.reps+3)

lomb.fluc<-list()
peri.fluc<-list()
mtap.fluc<-list()
haar.fluc<-list()



# This loop goes through the list of parameter combinations (params) and estimates the scaling exponent for the three methods for n.reps random series
for(j in 1:length(params[,1])){
  print(params[j,])
  print(proc.time()[3]-ptm[3])
  p5<-params[j,3]/3
  p4<-params[j,3]/4
  p2<-exp((log(p5)-log(2))/2+log(2))
  p3<-exp((log(p5)-log(2))/2*1.5+log(2))
  p1<-exp((log(p5)-log(2))/2*.5+log(2))
#  print(c(p1,p2,p3,p4,p5))
  if(params[j,2]>=0){
    fitranges<-rbind(c(2,p2),c(p2,p5),c(p1,p3),c(2,p5),c(4,p4))*5120/params[j,3]
print(fitranges)
   # fitranges<-rbind(c(2,9.2),c(9.2,42.7),c(4.3,19.8),c(2,43),c(4,32))*40
    dts<-lapply(1:n.reps,function(i)GenerateTime(dt=5120/params[j,3],tmin=5120/params[j,3],tmax=5120,skew=params[j,2]))
    zser.fs<-lapply(1:n.reps,function(i)
      Paleoseries(dts = dts[[i]],dt=5120/params[j,3],H = params[j,1],sim = sim.method,tau = 2*5120/params[j,3],blockwidth = 0.1,seed = seedz[i])$Irregular
    )
    zser.ba<-lapply(1:n.reps,function(i)
      Paleoseries(dts = dts[[i]],dt=5120/params[j,3],H = params[j,1],sim = sim.method,seed = seedz[i])$Irregular
    )
  }else{
    fitranges<-rbind(c(2,p2),c(p2,p5),c(p1,p3),c(2,p5),c(4,p4))
    print(fitranges)
    #fitranges<-rbind(c(2,9.2),c(9.2,42.7),c(4.3,19.8),c(2,43),c(4,32))
    if(sim.method=="fn") zser.fs<-lapply(1:n.reps,function(i) FractionalNoise(nn = params[j,3],H = params[j,1]))
    if(sim.method=="spl") zser.fs<-lapply(1:n.reps,function(i) SimPowerlawH(N = params[j,3],H = params[j,1]))
    zser.ba<-zser.fs}

  for(mt in 1:2){
    zser<-lapply(1:n.reps,function(i) if(mt==1){zser.ba[[i]]}else{zser.fs[[i]]})
    lomb.fluc[[mt]]<-lapply(1:n.reps,function(i) if(params[j,2]>=0 & unsinc ){UnSinc(LombScargle(zser[[i]],scales=c(0.5,1)),5120/params[j,3])}else{LombScargle(zser[[i]],scales=c(0.5,1))})
    mtap.fluc[[mt]]<-lapply(1:n.reps,function(i) if(params[j,2]>=0 & unsinc){UnSinc(Spectrum(zser[[i]],LowPass=LwPss,scales=c(0.5,1)),5120/params[j,3])}else{Spectrum(zser[[i]],LowPass=LwPss,scales=c(0.5,1))})
    peri.fluc[[mt]]<-lapply(1:n.reps,function(i) if(params[j,2]>=0 & unsinc){UnSinc(Spectrum(zser[[i]],LowPass=LwPss,scales=c(0.5,1),method = 'periodogram'),5120/params[j,3])}else{Spectrum(zser[[i]],LowPass=LwPss,scales=c(0.5,1),method = 'periodogram')})
    haar.fluc[[mt]]<-lapply(1:n.reps,function(i) Haar(zser[[i]],scales=c(0.5,1)))

  }

  for(fittos in 1:5){
    if(fittos==1){rng<-fitranges[1,]}else{rng<-fitranges[2,]}
    rng<-fitranges[fittos,]
    fitty<-"glm"
    H.lomb<-list()
    H.mtap<-list()
    H.haar<-list()
    H.peri <-list()
    for(mt in 1:2){

      H.mtap[[mt]]<-sapply(1:n.reps,function(i) round(defaulttry(Fit(mtap.fluc[[mt]][[i]],fit=fitty,scales=rng)$H,Fit(mtap.fluc[[mt]][[i]],fit='lm',scales=rng)$H),6))
      H.haar[[mt]]<-sapply(1:n.reps,function(i) round(defaulttry(Fit(haar.fluc[[mt]][[i]],fit=fitty,scales=rng)$H,Fit(haar.fluc[[mt]][[i]],fit='lm',scales=rng)$H),6))
      H.peri[[mt]]<-sapply(1:n.reps,function(i) round(defaulttry(Fit(peri.fluc[[mt]][[i]],fit=fitty,scales=rng)$H,Fit(peri.fluc[[mt]][[i]],fit='lm',scales=rng)$H),6))
      H.lomb[[mt]]<-sapply(1:n.reps,function(i) round(defaulttry(Fit(lomb.fluc[[mt]][[i]],fit=fitty,scales=rng)$H,Fit(lomb.fluc[[mt]][[i]],fit='lm',scales=rng)$H),6))
      }

    if(fittos==1){
      Hresults.sts[j,]<-c(as.vector(apply(params[j,],1,as.numeric)),
                         #H.peri[[1]],
                         as.vector( c(H.lomb[[1]],H.lomb[[2]],H.mtap[[1]],H.mtap[[2]],H.peri[[1]],H.peri[[2]],H.haar[[1]],H.haar[[2]]
                                      #      ,H.dfa[[1]],H.dfa[[2]]
                         ))
      )}
     if(fittos==2){
        Hresults.lts[j,]<-c(as.vector(apply(params[j,],1,as.numeric)),
                            #H.peri[[1]],
                            as.vector( c(H.lomb[[1]],H.lomb[[2]],H.mtap[[1]],H.mtap[[2]],H.peri[[1]],H.peri[[2]],H.haar[[1]],H.haar[[2]]
                                         #      ,H.dfa[[1]],H.dfa[[2]]
                            ))
        )
     }
    if(fittos==3){
      Hresults.mts[j,]<-c(as.vector(apply(params[j,],1,as.numeric)),
                          #H.peri[[1]],
                          as.vector( c(H.lomb[[1]],H.lomb[[2]],H.mtap[[1]],H.mtap[[2]],H.peri[[1]],H.peri[[2]],H.haar[[1]],H.haar[[2]]
                                       #      ,H.dfa[[1]],H.dfa[[2]]
                          ))
      )
    }
    if(fittos==4){
      Hresults.ats[j,]<-c(as.vector(apply(params[j,],1,as.numeric)),
                          #H.peri[[1]],
                          as.vector( c(H.lomb[[1]],H.lomb[[2]],H.mtap[[1]],H.mtap[[2]],H.peri[[1]],H.peri[[2]],H.haar[[1]],H.haar[[2]]
                                       #      ,H.dfa[[1]],H.dfa[[2]]
                          ))
      )
    }
    if(fittos==5){
      Hresults.bts[j,]<-c(as.vector(apply(params[j,],1,as.numeric)),
                          #H.peri[[1]],
                          as.vector( c(H.lomb[[1]],H.lomb[[2]],H.mtap[[1]],H.mtap[[2]],H.peri[[1]],H.peri[[2]],H.haar[[1]],H.haar[[2]]
                                       #      ,H.dfa[[1]],H.dfa[[2]]
                          ))
      )
    }

  }
}



# Read the data as a tibble for easier manipulation
Hresults.sts<-dplyr::as_tibble(Hresults.sts)
#Hresults<-as.data.frame(Hresults)
# We give proper names to the columns
names(Hresults.sts)<-c("H","Skew","Npts",#sapply(1:n.reps,function(i) paste("Peri.linfit",i)),
                      as.vector(c(
                        sapply(1:n.reps,function(i) paste("Lomb.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Lomb.Fltr",i,sep=".")),
                        sapply(1:n.reps,function(i) paste("Spec.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Spec.Fltr",i,sep=".")),
                        sapply(1:n.reps,function(i) paste("Peri.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Peri.Fltr",i,sep=".")),
                        sapply(1:n.reps,function(i) paste("Haar.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Haar.Fltr",i,sep="."))
                        #   ,sapply(1:n.reps,function(i) paste("DFA.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("DFA.Fltr",i,sep="."))
                      ))

)

Hresults.lts<-dplyr::as_tibble(Hresults.lts)
#Hresults<-as.data.frame(Hresults)
# We give proper names to the columns
names(Hresults.lts)<-c("H","Skew","Npts",#sapply(1:n.reps,function(i) paste("Peri.linfit",i)),
                       as.vector(c(
                         sapply(1:n.reps,function(i) paste("Lomb.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Lomb.Fltr",i,sep=".")),
                         sapply(1:n.reps,function(i) paste("Spec.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Spec.Fltr",i,sep=".")),
                         sapply(1:n.reps,function(i) paste("Peri.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Peri.Fltr",i,sep=".")),
                         sapply(1:n.reps,function(i) paste("Haar.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Haar.Fltr",i,sep="."))
                         #   ,sapply(1:n.reps,function(i) paste("DFA.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("DFA.Fltr",i,sep="."))
                       ))

)

Hresults.mts<-dplyr::as_tibble(Hresults.mts)
#Hresults<-as.data.frame(Hresults)
# We give proper names to the columns
names(Hresults.mts)<-c("H","Skew","Npts",#sapply(1:n.reps,function(i) paste("Peri.linfit",i)),
                       as.vector(c(
                         sapply(1:n.reps,function(i) paste("Lomb.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Lomb.Fltr",i,sep=".")),
                         sapply(1:n.reps,function(i) paste("Spec.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Spec.Fltr",i,sep=".")),
                         sapply(1:n.reps,function(i) paste("Peri.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Peri.Fltr",i,sep=".")),
                         sapply(1:n.reps,function(i) paste("Haar.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Haar.Fltr",i,sep="."))
                         #   ,sapply(1:n.reps,function(i) paste("DFA.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("DFA.Fltr",i,sep="."))
                       ))

)

Hresults.ats<-dplyr::as_tibble(Hresults.ats)
#Hresults<-as.data.frame(Hresults)
# We give proper names to the columns
names(Hresults.ats)<-c("H","Skew","Npts",#sapply(1:n.reps,function(i) paste("Peri.linfit",i)),
                       as.vector(c(
                         sapply(1:n.reps,function(i) paste("Lomb.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Lomb.Fltr",i,sep=".")),
                         sapply(1:n.reps,function(i) paste("Spec.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Spec.Fltr",i,sep=".")),
                         sapply(1:n.reps,function(i) paste("Peri.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Peri.Fltr",i,sep=".")),
                         sapply(1:n.reps,function(i) paste("Haar.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Haar.Fltr",i,sep="."))
                         #   ,sapply(1:n.reps,function(i) paste("DFA.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("DFA.Fltr",i,sep="."))
                       ))

)

Hresults.bts<-dplyr::as_tibble(Hresults.bts)
#Hresults<-as.data.frame(Hresults)
# We give proper names to the columns
names(Hresults.bts)<-c("H","Skew","Npts",#sapply(1:n.reps,function(i) paste("Peri.linfit",i)),
                       as.vector(c(
                         sapply(1:n.reps,function(i) paste("Lomb.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Lomb.Fltr",i,sep=".")),
                         sapply(1:n.reps,function(i) paste("Spec.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Spec.Fltr",i,sep=".")),
                         sapply(1:n.reps,function(i) paste("Peri.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Peri.Fltr",i,sep=".")),
                         sapply(1:n.reps,function(i) paste("Haar.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("Haar.Fltr",i,sep="."))
                         #   ,sapply(1:n.reps,function(i) paste("DFA.BAvg",i,sep=".")),sapply(1:n.reps,function(i) paste("DFA.Fltr",i,sep="."))
                       ))

)

# Export the results in a easy to read text file
#write.table(x = Hresults, file = "~/Documents/RProjects/RScaling/data/Hresults100FitRanges16Unsinc.txt",col.names = FALSE,row.names = FALSE)
Hresults.fit<-list("ShortTS"=Hresults.sts,"LargeTS"=Hresults.lts,"MiddleTS"=Hresults.mts,"AllTS"=Hresults.ats,"BestTS"=Hresults.bts)
print("ToSave")
save(Hresults.fit,file = paste(sourcefolder,"data",filename,sep = "/"))

# Import table after export
#Hresults100<-as.tibble(read.table("~/Hresults100.txt"))

# If done in parallel, this shuts down the cluster
#stopCluster(cl)


# This prints the time taken to compute
tm<-proc.time()-ptm
print(tm)
