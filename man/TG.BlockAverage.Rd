% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/TG.BlockAverage.R
\name{TG.BlockAverage}
\alias{TG.BlockAverage}
\title{Block average a time series with either consecutive or non-consecutive blocks}
\usage{
TG.BlockAverage(
  dts,
  tsin,
  blockwidth = NULL,
  meandtsout = TRUE,
  truncated.Gaussian = TRUE,
  drop.Gauss = 0.5
)
}
\arguments{
\item{dts}{Middle points of the time steps for the block averaged process}

\item{tsin}{Higher resolution process to be block averaged, should be a zoo object}

\item{blockwidth}{block width for the sampling points dts. Defaults to NULL (consecutive blocks). If a numeric value is given, this is taken as the width (in dts units) around the dts-midpoints, if a vector (of the same length as dts) is given, these are the widthes around dts for the sampling}

\item{meandtsout}{Logical when TRUE, returns the mean time of time steps present for each block, but when FALSE, assigns directly the value of input dts as output dts}

\item{truncated.Gaussian}{Logical}
}
\value{
\code{zoo}-object with the block-averaged time series
}
\description{
Block average a time series with either consecutive or non-consecutive blocks
}
\examples{
dts<-seq(5,1050,by=55)
tsin<-zoo::zoo(rnorm(1000),order.by=seq(1,1000,by=1))

ts.noncons<-BlockAverage(dts=dts,tsin=tsin,blockwidth=20)
ts.cons<-BlockAverage(dts=dts,tsin=tsin)
ts.noncons.changing<-BlockAverage(dts=dts,tsin=tsin,blockwidth=seq(5,50,length.out=length(dts)))
plot(tsin)
lines(ts.noncons,col="blue",lwd=2)
lines(ts.cons,col="cyan")
lines(ts.noncons.changing,col="green")
}
\author{
Kira Rehfeld (added nonconsecutive option) & Raphaël Hébert (consecutive original), \email{rphlhbrt@gmail.com}
}
