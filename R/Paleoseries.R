#' Paleoseries Generator
#'
#' @param dts Input timesteps which can be irregular
#' @param H Scaling exponent used to generate the surrogates
#' @param dt Resolution at which to interpolate the regular version
#' @param sim Simulation function, "spl" for SimPowerlawH and "fn" for FractionalNoise
#' @param tau Tau is the time constant of the exponential smoothing
#' @param blockwidth block width for the sampling points dts. Defaults to NULL (consecutive blocks). If a numeric value is given, this is taken as the width (in dts units) around the dts-midpoints, if a vector (of the same length as dts) is given, these are the widths around dts for the sampling
#' @param trend Add a trend of the given value in units/year
#' @param pad Length of the low-pass filter in LowPass, has to be odd
#'
#' @return Irregular and regular series, as well as the corresponding "annual" series
#' @export
#' @importFrom zoo index
#' @examples
Paleoseries<-function (dts, H, dt = NULL, sim = "spl", seed = NULL, tau = NULL,
                       blockwidth = NULL, pad = NULL, trend = 0)
{
  if (is.null(dt)) {
    dt <- round(mean(diff(dts)))
  }
  if (!is.null(seed))
    set.seed(seed)
  if (is.null(pad))
    pad <- 10 * max(diff(dts))
  if (round(pad/2) == pad/2)
    pad <- pad + 1
  ptA <- floor(dts[1] - max(diff(dts))) - 1 * pad
  ptB <- ceiling(dts[length(dts)] + max(diff(dts))) + 1 *
    pad
  shift <- if (is.null(tau)) {
    0
  }
  else {
    1 * pad - dts[1]
  }
  shift <- 0
  if (sim == "fn")
    annual.series <- FractionalNoise(nn = (ptB - ptA + 1),
                                     H = H, dts = ptA:ptB + shift) + (1:(ptB - ptA +
                                                                           1)) * trend
  if (sim == "spl")
    annual.series <- SimPowerlawH(N = (ptB - ptA + 1), H = H,
                                  dts = ptA:ptB + shift) + (1:(ptB - ptA + 1)) * trend
  if (!is.null(tau)) {
    fltr <- PaleoSpec::Lowpass(omega.c = 1/tau, n = pad)
    fltred.series <- zoo::zoo(as.vector(PaleoSpec::ApplyFilter(zoo::coredata(annual.series),
                                                               fltr)), order.by = (ptA:ptB) + shift)
  }
  else {
    fltred.series <- annual.series
  }
  zser <- MakeEquidistant(dts, rnorm(length(dts)), dt = dt)
  ttst <<- zser
  dts.reg <- zoo::index(zser)
  kept.fltred <- fltred.series[which(min(dts) < zoo::index(fltred.series) &
                                       max(dts) > zoo::index(fltred.series))]
  min.ann <- min(zoo::index(kept.fltred))
  max.ann <- max(zoo::index(kept.fltred))
  keep <- which(zoo::index(annual.series) >= min.ann & zoo::index(annual.series) <=
                  max.ann)
  fltred.series <- fltred.series[which(!is.na(fltred.series))]
  return(list(Irregular = BlockAverage(dts, fltred.series,
                                       blockwidth = blockwidth), Regular = BlockAverage(dts.reg,
                                                                                        fltred.series, blockwidth = blockwidth), Annual = annual.series[keep],
              Filtered = fltred.series))
}
