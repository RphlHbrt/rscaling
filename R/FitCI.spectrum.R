#' @title Fit Spectrum Analysis with Confidence Interval
#'
#' @param fluced Object of class 'spectrum'
#' @param n.reps Number of replicas for building the confidence intervals
#' @param width Positive real which defines the width of the binning for the fit
#' @param dt Positive real which defines the resolution to which one should reinterpolate the series at regular resolution
#' @param plot Logical to display the fit line or not
#' @param scales Pair of real numbers which define minimum and maximum scales betweeen which to fit
#' @param binline Logical to display the binned data
#' @param n.sd Number of standard deviations for the confidence interval, 1.645 corresponds to 90 percent CI for example
#' @param col Colour of fit line if plot true
#' @param lwd Width of fit line if plot true
#' @param lty Type of fit line if plot true
#' @importFrom multitaper spec.mtm
#' @importFrom zoo is.regular is.zoo zoo index coredata
#'
#' @return list
#' @author Raphaël Hébert, \email{rphlhbrt@@gmail.com}, Sep 2018
#' @export
#'
#'
#'
#'
FitCI.spectrum<-function(fluced,n.reps=25,width=NULL,dt=NULL,plot=FALSE,scales=NULL,binline=FALSE,n.sd=1,col='blue',lwd=2,lty=1){
if(class(fluced)!="spectrum"){print("Input spectrum class object")}
  # The number of replicas for the confidence interbal n.reps should be an integer
  n.reps<-round(n.reps)
  require(multitaper)
  # It should also be at least 2
  if(n.reps<2){n.reps<-2}
  # Initial guess
  H0<-Fit(fluced,width=width,plot=plot,scales=scales,binline=binline,col=col,lwd=lwd,lty=lty)$H
  beta0<-1+2*H0
  # Sampling of the initial series
  dts<-fluced$Age
  # Number of points required for the replicate series
  ddts<-diff(dts)
  nn<-round(max(dts)+ddts[1]/2+ddts[length(ddts)]/2+1)
  # We use the full amount of timescales to estimate the H used to generate the replicates
  H0.reps<-Fit(fluced,width=width,plot=plot,scales=NULL,binline=binline,col=col,lwd=lwd,lty=lty)$H
  # We measure the scaling exponent for random replicates
  beta.replicates<-sapply(1:n.reps,function(i)Fit(Spectrum(BlockAverage(dts,FractionalNoise(nn,H0.reps,dts=1:nn+round(dts[1])-round(ddts[1]/2)-1)),scale=c(min(fluced$Scales),max(fluced$Scales))),width=width,plot=plot,scales=scales,binline=binline,lwd=lwd,lty=lty)$Beta)
  beta<-2*beta0-mean(beta.replicates)
  betasd<-sd(beta.replicates)
  return(list("Beta"=beta,"Beta.CI"=c(beta-n.sd*betasd,beta+n.sd*betasd),"Beta0"=beta0,"Beta.bias"=mean(beta.replicates)-beta0,"Beta.reps"=beta.replicates,"H"=(beta-1)/2,"H.CI"=(c(beta-n.sd*betasd,beta+n.sd*betasd)-1)/2,"H0"=H0,"H.bias"=(mean(beta.replicates)-1)/2-H0,"H.reps"=(beta.replicates-1)/2))










}
