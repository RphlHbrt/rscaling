#' Optimal Fit Range Finder for DFA Fitting
#'
#' @param zser zoo timeseries of interest
#' @param dt default scale to interpolate at
#' @param n.reps Number of replicas to make for the average spectra
#' @param plot Logical to display the diagnotic plot
#' @param H0 Specify the scaling exponent used for the replicas directly
#' @importFrom zoo index
#' @return Optimal Resolution to avoid power loss smaller than the thresh value
#' @export
#'
#' @examples
OptimalRangeDFA<-function(zser,scale=5,dt=NULL, n.reps=5, plot=FALSE,H0=NULL){
  if(is.null(H0)){H0<-Fit(DFA(zser = zser,scale = scale))$H}

  surr<-lapply(1:n.reps,function(i)RScaling::Paleoseries(zoo::index(zser),H0,dt=dt))
  irreg<-list()
  reg<-list()
  min.scale<-round(mean(diff(zoo::index(zser)))/4)
  max.scale<-0.5*(max(zoo::index(zser))-min(zoo::index(zser)))
  scales<-2^seq(log2(min.scale),log2(max.scale),0.2)
  for(i in 1:n.reps)  {
    rdn.sd<-abs(round(10000*rnorm(1)))
    set.seed(rdn.sd)
    irreg[[i]]<-DFA(surr[[i]][[1]],scale=scales,dt=max(min.scale/6,1))
  #  set.seed(rdn.sd)
  #  reg[[i]]<-DFA(surr[[i]][[3]],scale=scales,dt=1)
  }
 # mn.sc<-2^(log2(4*min.scale))
 # mx.sc<-2^(log2(4*min.scale)+8)
 # rev.scales<-rev(irreg[[1]]$Scales[which(irreg[[1]]$Scales>mn.sc & irreg[[1]]$Scales < mx.sc)])
  rev.scales<-rev(irreg[[1]]$Scales)
  Hs.irreg<-sapply(1:n.reps,function(i)
    sapply(3:length(rev.scales),function(j)
      Fit(irreg[[i]],scales=c(rev.scales[j],max.scale))$H))
  Hs.irreg<-as.data.frame(Hs.irreg)
  teal<-  '#01665e'
  brown<- '#8c510a'
  colpal<-colorRampPalette(colors = c(teal,brown))
  BiasSdCurve(rep(H0,length(rev.scales)-2),Hs.irreg,method = "V",colpal(length(rev.scales)-2),add=FALSE,legend = FALSE)
  legend('topleft',legend=round(rev.scales[-(1:2)]),col='black',pch=21,pt.bg = colpal(length(rev.scales)-2))

}
