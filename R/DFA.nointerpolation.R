DFA.nointerpolation<-function(zser,scale=NULL,q=1,overlap=0){


  if (!zoo::is.zoo(zser)) warning("Input a zoo object, or things may not work correctly.")
  #Extract data and time from zoo object
  timevec<-zoo::index(zser)
  t0<-timevec
  dat<-zoo::coredata(zser)
  len.ts<-length(dat)

  if(overlap>=1){overlap=0.99}

  #  scale<-if(is.null(scale)){seq(4*mean(diff(index(zser))),0.5*(max(index(zser))-min(index(zser))),mean(diff(index(zser))))}else{scale}
  scale<-if(is.null(scale)){2^seq(log2(3*mean(diff(zoo::index(zser)))),log2(0.5*(max(zoo::index(zser))+1-min(zoo::index(zser)))),0.2)}else{scale}

  scale<-scale[which(scale<=(t0[len.ts]-t0[1]+1))]
  scale<-scale[which(scale>(min(diff(t0))))]

  # Initialize Haar fluctuations vector Hfluc
  rmsfluc<-numeric(length(scale))



  #This loop produces the mean qth order fluctuation at every scale contained in the input vector scale
  for(i in 1:length(scale)){
    #Few constant values used
    largestscale<-t0[len.ts]
    scali<-scale[i]
    scali2<-scali/2
    maxfluc<-floor((largestscale-t0[1]+1)/scali)
    #seqa is 1st point of each fluctuation interval. If overlap is 0, then they don't overlap.
    seqa<-seq(t0[1],largestscale,(1-overlap)*scali)

    getRMSresiduals<-function(x,y){
      return(sqrt(mean((lm(y~x)$residuals)^2)))

    }

    #Below we find the residual values for each intervals.
    rms.fit.residuals<-sapply(1:(length(seqa)-1),function(j) getRMSresiduals(t0[which(t0>=seqa[j]&t0<(seqa[j+1]))],dat[which(t0>=seqa[j]&t0<(seqa[j+1]))]))



      rmsfluc[i]<-mean(abs(rms.fit.residuals)^q,na.rm = TRUE)^(1/q)



  }
  dfaed<-list("Scales"=scale,"Fluc"=rmsfluc,"Freq"=1/scale,"Age"=zoo::index(zser))
  class(dfaed)<-"dfa"
  return(dfaed)
}
