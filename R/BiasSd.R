#' @title Bias-Standard Deviation
#' @param varvec Vector of the variable to plot sequentially
#' @param H.matrix Matrix of the estimated metric, rows correspond to the values of varvec in the same order
#' @param method Method to display. The function scans for column names starting with this name and includes those for calculating the test statistics
#' @param fit Fit method to display
#' @author Raphaël Hébert, \email{rphlhbrt@@gmail.com}, Sep 2018
#' @importFrom graphics lines
#' @return Curve
#' @export
#'
#' @examples
BiasSd<-function(varvec,H.matrix,method,fit="linfit"){

  npts<-length(varvec)
  curve.matrix<-matrix(0,npts,3)
  meth.ind<-which(grepl(method,colnames(H.matrix)) & grepl(fit,colnames(H.matrix)))
  #  print(length(meth.ind))
  for(i in 1:npts){
    curve.matrix[i,]<-c(varvec[i],
                        mean(as.numeric(H.matrix[i,meth.ind]))-varvec[i],sd(as.numeric(H.matrix[i,meth.ind])))

  }



  return(curve.matrix)
}
