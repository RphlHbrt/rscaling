#' @title Plot function for 'haar' class objects
#'
#' @param speced Object of class 'haar'
#' @param freq Logical to plot as a function of frequency (if TRUE) rather than the default timescale
#' @param add Logical to make a new plot or simply add a curve to an existing plot
#' @param xlab Text for custom x label
#' @param ylab Text for custom y label
#' @param ... parameters to be passed to generic plot function
#' @return plot
#' @export
#' @author Raphaël Hébert, \email{rphlhbrt@@gmail.com}, Sep 2018
#' @importFrom grDevices adjustcolor
#' @importFrom graphics plot points
#' @importFrom stats sd
#' @importFrom latex2exp TeX
#' @examples
plot.haar<-function(speced,add=FALSE,freq=FALSE,xlab=NULL,ylab=NULL,...){

  if(freq){
    x<-log10(speced$Freq)
    y<-log10(speced$Fluc)
    xlab<-if(is.null(xlab)){latex2exp::TeX("$log_{10} \\omega $")}else{xlab}
    ylab<-if(is.null(ylab)){latex2exp::TeX("$log_{10}S_1(\\Delta t)$")}else{ylab}
  }else{
    x<-log10(speced$Scales)
    y<-log10(speced$Fluc)
    xlab<-if(is.null(xlab)){latex2exp::TeX("$log_{10} \\Delta t $")}else{xlab}
    ylab<-if(is.null(ylab)){latex2exp::TeX("$log_{10}S_1(\\Delta t)$")}else{ylab}
    }

    if(!add){plot(x,y,xlab=xlab,ylab=ylab,...)
    }else{
      points(x,y,...)
      }


}
